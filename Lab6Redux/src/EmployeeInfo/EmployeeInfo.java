package EmployeeInfo;
public class EmployeeInfo {
	public String[] EmpName;
	public double[] reGhours;
	public double[] OThours;
	public double[] reGpay;
	public double[] OTpay;
	public double[] GrossPay;
	public void getInfo() {
		File info = new File();
		calcPay calc = new calcPay();
		String[] strEmpName = info.EmpID;
		double[] regHours = calc.regHours;
		double[] OTHours = calc.overTime;
		double[] regPay = calc.RegPay;
		double[] OTPay = calc.OTPay;
		double[] grossPay = calc.grossPay;
		setInfo(strEmpName, regHours, OTHours, regPay, OTPay, grossPay);
	}// getInfo
	public void setInfo(String[] strEmpName, double[] regHours, double[] OTHours, double[] regPay, double[] OTPay, double[] grossPay ) {
		EmpName = strEmpName;
		reGhours = regHours;
		OThours = OTHours;
		reGpay = regPay;
		OTpay = OTPay;
		GrossPay = grossPay;
	}// setInfo
	
}// class
