package EmployeeInfo;
import java.io.*;
import java.util.*;
public class File {
	private final int MAX = 10;
	public String[] EmpID = new String[MAX];
	public String[] Lname = new String[MAX];
	public String[] Fname = new String[MAX];
	public double[] Wages = new double[MAX];
	public double[] Hours = new double[MAX];
	public void getFile() throws FileNotFoundException {
		int nCount = 0;
		String[] strEmpID = new String[MAX];
		String[] strLName = new String[MAX];
		String[] strFName = new String[MAX];
		double[] dWage = new double[MAX];
		double[] dHours = new double[MAX];
		FileReader info = new FileReader("Employees.txt");
		@SuppressWarnings("resource")
		Scanner input = new Scanner(info);
		input.useDelimiter(",");
		while(input.hasNext()) {
			strEmpID[nCount] = input.next();
			strLName[nCount] = input.next();
			strFName[nCount] = input.next();
			dWage[nCount] = Double.parseDouble(input.next());
			dHours[nCount] = Double.parseDouble(input.next());
			nCount++;
		}// while	
		EmpID = strEmpID;
		Lname = strLName;
		Fname = strFName;
		Wages = dWage;
		Hours = dHours;
		calcPay math = new calcPay();
	}// getFile
}// class
