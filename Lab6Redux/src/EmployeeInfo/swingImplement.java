package EmployeeInfo;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
@SuppressWarnings("serial")
public class swingImplement extends JFrame {
	swingInternals printer = new swingInternals();
	private static final int FRAME_WIDTH = 700;
	private static final int FRAME_HEIGHT = 500;
	private JTextField empID;
	private JTextField payFrequency;
	private JTextField empInfo;
	public int choice;
	public String x = printer.strDisplay;
	public swingImplement() {
		infoPrint print = new infoPrint();
		empID = new JTextField("Employee ID Number: "+ print.getIDChoice(empID.getText()));
		payFrequency = new JTextField("Enter pay frequency; [Biweekly], etc..."+ printer.getFrequencyString(payFrequency.getText()));
		empInfo = new JTextField("Employee Summary "+ x);
		empInfo.setEditable(false);
		add(empID, BorderLayout.NORTH);
		add(payFrequency, BorderLayout.NORTH);
		add(empInfo, BorderLayout.CENTER);
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		buttonPanel();
	}// swing

	public void buttonPanel() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(5,5));
		buttonPanel.add(operatorButton("Show Report"));
		buttonPanel.add(operatorButton("Clear"));
		buttonPanel.add(operatorButton("Close"));
		add(buttonPanel, BorderLayout.SOUTH);
		
	}// buttonPanel
	public JButton operatorButton(String op) {
		JButton button = new JButton(op);
		ActionListener listener = new swingInternals(op);
		button.addActionListener(listener);
		return button;
	}// operatorButton

}// class
